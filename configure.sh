#!/bin/bash

# Install brew
if test ! $(which brew)
then
    echo " Installing Homebrew for you."
    
    # Install the correct Homebrew for MacOS or Linux
    if test "$(uname)" = "Darwin"
    then
        ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    elif test "$(expr substr $(uname -s) 1 5)" = "Linux"
    then
        ruby -e "$(ccurl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install)"
    fi
else
    echo " Homebrew is already installed."
fi

echo "Updating package lists..."
brew update

# zsh install
echo " Checking whether zsh is installed"
if [ $(which zsh) == "/usr/local/bin/zsh" ]
then
    echo ''
    echo "zsh already installed...skipping"
else
    echo 'zsh not found, installing...'
    brew install zsh zsh-completions
fi

# change to use zsh as default shell (will need to enter mac's password)
echo " changing default shell to zsh."
if [ $(which zsh) == "/usr/local/bin/zsh" ]
then
    echo " Using installed zsh."
else
    chsh -s /usr/local/bin/zsh
    echo " zsh will be the default shell for any new terminal sessions"
fi

# oh-my-zsh install
echo " Installing oh-my-zsh."
if [ -d ~/.oh-my-zsh/ ]
then
    echo ''
    echo ' oh-my-zsh is already installed...'
    read -p "Would you like to update oh-my-zsh now?" -n 1 -r
    echo ''
        if [[ $REPLOY =~ ^[Yy]$ ]]
        then
            cd ~/.oh-my-zsh && git pull
            if [[ $? -eq 0 ]]
            then
                echo "Update complete..." && cd
            else
                echo "Update not complete..." >&2 cd
            fi
        else
            echo " Not updating"
        fi
else
    echo "oh-my-zsh not found, installing..."
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi

# oh-my-zsh plugin
echo ''
echo "Now installing oh-my-zsh plugins..."
echo ''
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

# nerd Fonts
echo " Now installing nerd fonts."
brew tap homebrew/cask-fonts
brew cask install font-hack-nerd-font

# powerlevel9k install
echo ''
echo "Now installing powerlevel9k..."
echo ''
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

# iTerm app install
echo ''
echo " Now installing iTerm2"
echo ''
if [ "$TERM_PROGRAM" =~ "iTerm" ]
then
    echo " iTerm2 already installed.  Nothing done"
else
    sh -c "brew cask install iTerm2"
    echo "iTerm2 installed"
fi

# iTerm config copy
echo ''
echo " Updating iTerm2 with current config"
sh -c " defaults write com.googlecode.iterm2.plist PrefsCustomFolder -string "~/Development/dotfiles/iTerm2""
sh -c " defaults write com.googlecode.iterm2.plist LoadPrefsFromCustomFolder -bool true"

# copy .zsh configuration files
sh -c "cp ./.zshrc ~/"
sh -c "source ~/.zshrc"
echo " zsh profile added"

# pathogen install

# nerdtree for vim install


