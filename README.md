# dotfiles

Location for personal dotfiles that can be used to easily configure terminals and machines with common aliases etc.

## Assumptions
iTerm2 has been installed
Nerd Fonts have been installed - specifically the font-hack-nerd-font from brew
Zsh is the existing shell (currently working through the bash_profile options)


Current .zshrc file assumes that oh-my-zsh is installed

Additional things to verify:
iTerm2 did not appear to set the Non-ASCII Font correctly.  If you are missing the images in the command prompt, check
the Profiles-git profile and make sure that Use a different font for non-ascii text is set to Hack Nerd Font.

## Thanks
I was first introduced into customizations based on a presentation that [jldeen](https://github.com/jldeen/dotfiles)
did called [Pizza vs DevOps: Setting up a pipeline in 1hr or less](https://www.youtube.com/watch?v=MgTbI-SJMQ4&feature=youtu.be).  This repo is based off of the mac branch (no, not forked) as I liked her structure.
